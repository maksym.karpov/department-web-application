import unittest
from tests.base import BaseTestCase
from datetime import datetime


class TestModels(BaseTestCase):
    def test_employee(self):
        response_1 = self.client.put('/api/employee/1', data={'name': 'New Employee',
                                                              'dob': datetime.strptime('26.07.2001', '%d.%m.%Y').date(),
                                                              'salary': 10000,
                                                              'dep_name': 'New Department'})
        self.assertEqual(response_1.status_code, 201)
        self.assertIn(b'New Employee', response_1.data)
        self.assertIn(b'New Department', response_1.data)
        self.assertEqual(response_1.content_type, "application/json")

        response_2 = self.client.get('/api/employee/1')
        self.assertEqual(response_2.content_type, "application/json")

        response_3 = self.client.patch('/api/department/1', data={'name': 'Test Employee',
                                                                  'dob': datetime.strptime('26.07.2000',
                                                                                           '%d.%m.%Y').date(),
                                                                  'salary': 10000,
                                                                  'dep_name': 'Test Department'})
        self.assertEqual(response_3.content_type, "application/json")

        response_4 = self.client.delete('/api/department/1')
        self.assertEqual(response_4.status_code, 204)

        response_5 = self.client.get('/api/department/100')
        self.assertEqual(response_5.status_code, 404)
        self.assertEqual(response_5.content_type, "application/json")


if __name__ == '__main__':
    unittest.main()
