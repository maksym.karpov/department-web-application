import unittest
from tests.base import BaseTestCase


class TestModels(BaseTestCase):

    def test_department(self):
        response_1 = self.client.put('/api/department/1', data={'department': 'New Department'})
        self.assertEqual(response_1.status_code, 201)
        self.assertIn(b'New Department', response_1.data)

        response_2 = self.client.get('/api/department/1')
        self.assertEqual(response_2.content_type, "application/json")

        response_3 = self.client.patch('/api/department/1', data={'department': 'Test Department'})
        self.assertEqual(response_3.content_type, "application/json")
        self.assertIn(b'Test Department', response_3.data)

        response_4 = self.client.delete('/api/department/1')
        self.assertEqual(response_4.status_code, 204)

        response_5 = self.client.get('/api/department/100')
        self.assertEqual(response_5.status_code, 404)


if __name__ == '__main__':
    unittest.main()
