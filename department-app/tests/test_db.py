import unittest
from tests.base import BaseTestCase
from flask_department import db
from datetime import datetime
from flask_department.models.models import Employee, Department


class TestModels(BaseTestCase):
    def test_create_department(self):
        department = Department('Test Department')
        db.session.add(department)
        db.session.commit()
        self.assertEqual(department.id, 1)
        self.assertEqual(department.department, 'Test Department')

        assert department in db.session

    def test_create_employee(self):
        employee = Employee('Maksym Karpov', datetime.strptime('26.07.2001', '%d.%m.%Y').date(), 5000,
                            Department('Test Kharkiv Department'))
        db.session.add(employee)
        db.session.commit()

        first_employee = Employee.query.get(1)
        first_department = Department.query.get(1)
        self.assertEqual(first_employee.id, 1)
        self.assertEqual(first_employee.name, 'Maksym Karpov')
        self.assertEqual(first_employee.dob, datetime.strptime('26.07.2001', '%d.%m.%Y').date())
        self.assertEqual(first_employee.salary, 5000)
        self.assertEqual(employee.dep_id, 1)
        self.assertEqual(first_department.department, 'Test Kharkiv Department')

        assert first_employee in db.session


if __name__ == '__main__':
    unittest.main()
