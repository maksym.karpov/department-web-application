import unittest
from tests.base import BaseTestCase
from flask_department.models.models import Employee, Department
from flask_department import db


class TestModels(BaseTestCase):
    def test_index(self):
        response_1 = self.client.get("/")
        response_2 = self.client.get("/home")
        self.assertEqual(response_1.status_code, 200)
        self.assertEqual(response_2.status_code, 200)
        self.assertEqual(response_1.content_type, 'text/html; charset=utf-8')
        self.assertEqual(response_2.content_type, 'text/html; charset=utf-8')

    def test_list_departments(self):
        response = self.client.get("/ls_dep")
        self.assertEqual(response.status_code, 200)

        department = Department('Test Department')
        db.session.add(department)
        db.session.commit()

        response_2 = self.client.get("/del-dep/1")
        self.assertEqual(response_2.status_code, 302)

    def test_list_employees(self):
        response = self.client.get("/ls_emp")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'text/html; charset=utf-8')

    def test_list_search(self):
        response = self.client.get("/search")
        self.assertEqual(response.status_code, 200)

        employee = Employee('Test Employee', '05-05-2000', 5555, 'Test Department')
        db.session.add(employee)
        db.session.commit()

        response_1 = self.client.get('/delete/1')
        self.assertEqual(response_1.status_code, 302)


if __name__ == '__main__':
    unittest.main()
