class Config:
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'dev'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgres://efpqmdzpnxkmlk' \
                              ':0fd937656f5d24d2a8db40be7a6c8623350a648396260866be87f84f0103450c@ec2-3-248-4-172.eu' \
                              '-west-1.compute.amazonaws.com:5432/d5gkohj9ful3e4'


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgres://efpqmdzpnxkmlk' \
                              ':0fd937656f5d24d2a8db40be7a6c8623350a648396260866be87f84f0103450c@ec2-3-248-4-172.eu' \
                              '-west-1.compute.amazonaws.com:5432/d5gkohj9ful3e4'


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgres://fzemkpendmktrp' \
                              ':a0d30a47e21256c6bc8383ced50bb24a4f85bc94b091aa3d6f5928b33c820d62@ec2-52-212-157-46.eu' \
                              '-west-1.compute.amazonaws.com:5432/ddbuakt23igml8'
