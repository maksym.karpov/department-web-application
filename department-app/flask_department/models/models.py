from flask_department import db


class Department(db.Model):
    __tablename__ = 'departments'
    id = db.Column(db.Integer, primary_key=True)
    department = db.Column(db.String(45), nullable=False)

    employees = db.relationship('Employee', backref='dep_name')

    def __init__(self, department):
        self.department = department

    def __repr__(self):
        return f'{self.department}'


class Employee(db.Model):
    __tablename__ = 'employees'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(45), nullable=False)
    dob = db.Column(db.Date, nullable=False)
    salary = db.Column(db.Integer, nullable=False)

    dep_id = db.Column(db.Integer, db.ForeignKey('departments.id'))

    def __init__(self, name, dob, salary, dep_name):
        self.name = name
        self.dob = dob
        self.salary = salary
        self.dep_name = dep_name

    def __repr__(self):
        return f'{self.name}'
