from flask import render_template, Blueprint

bp = Blueprint('home', __name__)


@bp.route('/')
@bp.route('/home')
def index():
    return render_template('index.html')
