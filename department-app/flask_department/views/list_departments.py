from flask import request, redirect, url_for, flash, render_template, Blueprint
from flask_department.models.models import Employee, Department
from flask_department import db
bp = Blueprint('list_departments', __name__)


@bp.route('/ls_dep')
def ls_dep():
    average_salary = {}
    for dep in Department.query.all():
        avg_salary = db.session.query(db.func.round(db.func.avg(Employee.salary))).filter(
            Employee.dep_id == dep.id)
        average_salary.update({dep: avg_salary[0][0]})
    return render_template('ls_dep.html', average_salary=average_salary.items())


@bp.route('/new-dep/', methods=['POST'])
def new_dep():
    if request.method == 'POST':
        name = request.form['new-dep']
        data = Department(name)
        db.session.add(data)
        db.session.commit()

        flash('Department was successfully added')

        return redirect(url_for('list_departments.ls_dep'))


@bp.route('/edit-dep/', methods=['POST'])
def edit_dep():
    if request.method == 'POST':
        dep = Department.query.get(request.form.get('id'))
        dep.department = request.form['dep-name-edit']
        db.session.commit()

        flash('Department was successfully edited')

        return redirect(url_for('list_departments.ls_dep'))


@bp.route('/del-dep/<id_dep>', methods=['POST'])
def del_dep(id_dep):
    dep_id = Department.query.get(id_dep)
    db.session.delete(dep_id)
    db.session.commit()

    flash('Department was successfully deleted')

    return redirect(url_for('list_departments.ls_dep'))
