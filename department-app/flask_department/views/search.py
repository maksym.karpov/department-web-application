from flask import request, redirect, url_for, flash, render_template, Blueprint
from flask_department.models.models import Employee, Department
from flask_department import db

bp = Blueprint('search', __name__)


@bp.route('/search')
def search():
    data = Department.query.all()
    return render_template('search.html', all_dep=data)


@bp.route('/insert', methods=['POST'])
def insert():
    if request.method == 'POST':
        name = request.form['name']
        dep_name = Department.query.filter_by(department=request.form['department']).first()
        salary = request.form['salary']
        dob = request.form['dob-emp']

        data = Employee(name, dob, salary, dep_name)
        db.session.add(data)
        db.session.commit()

        flash('User was successfully added')

        return redirect(url_for('search.search'))


@bp.route('/edit/', methods=['GET', 'POST'])
def edit():
    if request.method == 'POST':
        data = Employee.query.get(request.form.get('id'))
        data.name = request.form['name']
        data.dob = request.form['dob-emp']
        data.salary = request.form['salary']
        data.dep_name = Department.query.filter_by(department=request.form['department-edit']).first()
        db.session.commit()

        flash('User was successfully edited')

        return redirect(url_for('search.search'))


@bp.route('/delete/<id>/', methods=['GET', 'POST'])
def delete(id):
    data = Employee.query.get(id)
    db.session.delete(data)
    db.session.commit()

    flash('User was successfully deleted')

    return redirect(url_for('search.search'))


@bp.route('/search_date', methods=['GET', 'POST'])
def search_date():
    if request.method == 'POST':
        date = request.form['date-dob']

        data = Employee.query.filter(date == Employee.dob).all()
        all_dep = Department.query.all()
        return render_template('search.html', emp_dob_date=data, all_dep=all_dep)


@bp.route('/search_period', methods=['GET', 'POST'])
def search_period():
    if request.method == 'POST':
        period_1 = request.form['period-1']
        period_2 = request.form['period-2']

        all_dep = Department.query.all()
        data = Employee.query.filter(Employee.dob.between(period_1, period_2))
        return render_template('search.html', emp_dob_period=data, all_dep=all_dep)
