from flask import render_template, Blueprint
from flask_department.models.models import Employee, Department
from flask_department import db

bp = Blueprint('list_employees', __name__)


@bp.route('/ls_emp')
def ls_emp():
    counter = {}
    info_list = []
    for dep in Department.query.all():
        emp_counter = db.session.query(Employee).filter(Employee.dep_id == dep.id).count()
        dep_names = dep.department
        emp_info = Employee.query.filter(Employee.dep_id == dep.id).all()

        counter.update({dep_names: emp_counter})
        info_list.append(emp_info)
    return render_template('ls_emp.html', counter=counter, info_list=info_list)
