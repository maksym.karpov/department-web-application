from flask_restful import Resource, reqparse, abort, fields, marshal_with
from flask_department.models.models import Employee, Department
from flask_department import db

emp_put_args = reqparse.RequestParser()

emp_put_args.add_argument('name', type=str, help='Employee name is required', required=True)

emp_put_args.add_argument('dob', type=str, help='Employee DOB is required', required=True)

emp_put_args.add_argument('salary', type=int, help='Employee salary is required', required=True)

emp_put_args.add_argument('dep_name', type=str, help='Department name is required', required=True)

emp_update_args = reqparse.RequestParser()

emp_update_args.add_argument('name', type=str, help='Employee name is required')

emp_update_args.add_argument('dob', type=str, help='Employee DOB is required')

emp_update_args.add_argument('salary', type=int, help='Employee salary is required')

emp_update_args.add_argument('dep_name', type=str, help='Department name is required')

resource_fields = {'id': fields.String,
                   'name': fields.String,
                   'dob': fields.String,
                   'salary': fields.Integer,
                   'dep_name': fields.String}


class Dep_Employees(Resource):
    @marshal_with(resource_fields)
    def get(self, emp_id):
        result = Employee.query.filter_by(id=emp_id).first()
        if not result:
            abort(404, message='Could not find Employee with that ID')

        return result

    @marshal_with(resource_fields)
    def put(self, emp_id):
        args = emp_put_args.parse_args()

        check_0 = Employee.query.filter_by(id=emp_id).first()
        check_1 = Department.query.filter_by(department=args['dep_name']).first()

        if check_0 or check_1:
            abort(409, message='Employee id or Department name is already taken!')

        emp = Employee(args['name'], args['dob'],
                       args['salary'], Department(args['dep_name']))
        db.session.add(emp)
        db.session.commit()

        return emp, 201

    @marshal_with(resource_fields)
    def patch(self, emp_id):
        args = emp_update_args.parse_args()

        result = Employee.query.filter_by(id=emp_id).first()
        check_0 = Department.query.filter_by(department=args['dep_name']).first()
        if check_0 or not result:
            abort(404, message='Error with Employee id or Department name, can not update!')

        if args['name']:
            result.name = args['name']
        if args['dob']:
            result.dob = args['dob']
        if args['salary']:
            result.salary = args['salary']
        if args['dep_name']:
            result.dep_name = Department(args['dep_name'])

        db.session.commit()

        return result

    def delete(self, emp_id):
        emp = Employee.query.filter_by(id=emp_id).first()
        if not emp:
            abort(404, message='Could not find Employee with that ID')

        db.session.delete(emp)
        db.session.commit()

        return '', 204
