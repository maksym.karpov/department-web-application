from flask_restful import Resource, reqparse, abort, fields, marshal_with
from flask_department.models.models import Department
from flask_department import db

dep_args = reqparse.RequestParser()
dep_args.add_argument('department', type=str, help='Department name is required', required=True)

resource_fields = {'id': fields.Integer,
                   'department': fields.String}


class Departments(Resource):
    @marshal_with(resource_fields)
    def get(self, dep_id):
        result = Department.query.filter_by(id=dep_id).first()
        if not result:
            abort(404, message='Could not find Department with that ID')

        return result

    @marshal_with(resource_fields)
    def put(self, dep_id):
        args = dep_args.parse_args()

        check_0 = Department.query.filter_by(id=dep_id).first()
        check_1 = Department.query.filter_by(department=args['department']).first()

        if check_0 or check_1:
            abort(404, message='Department id or Department name is already taken!')

        dep = Department(args['department'])
        db.session.add(dep)
        db.session.commit()

        return dep, 201

    @marshal_with(resource_fields)
    def patch(self, dep_id):
        args = dep_args.parse_args()

        result = Department.query.filter_by(id=dep_id).first()
        check_0 = Department.query.filter_by(department=args['department']).first()
        if check_0 or not result:
            abort(404, message='Error with Department id or Department name, can not update!')

        result.department = args['department']
        db.session.commit()

        return result

    def delete(self, dep_id):
        emp = Department.query.filter_by(id=dep_id).first()
        if not emp:
            abort(404, message='Could not find Department with that ID')

        db.session.delete(emp)
        db.session.commit()

        return '', 204
