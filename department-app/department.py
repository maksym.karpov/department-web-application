from flask_department.views import home, search, list_departments, list_employees
from flask_department.rest.emp_api import Dep_Employees
from flask_department.rest.dep_api import Departments
from flask_department import app, api

api.add_resource(Dep_Employees, '/api/employee/<int:emp_id>')
api.add_resource(Departments, '/api/department/<int:dep_id>')
app.register_blueprint(home.bp)
app.register_blueprint(list_departments.bp)
app.register_blueprint(list_employees.bp)
app.register_blueprint(search.bp)

if __name__ == '__main__':
    app.run()
