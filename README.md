# Department Web Application

## Introduction
Web application is created for managing departments and employees. Application allows to get access for all exsicting information about departments and employees working there. Department web application allows to calculate the avarege salary for each department, browse employees information and quickly find the information about employee by DOB parameter on a certain date or in the period between dates.

## HOME TAB
![HOME](https://gitlab.com/maksym.karpov/department-web-application/-/raw/master/images/Screenshot1.jpg)

## LIST OF EMPLOYEES TAB
![LIST OF EMPLOYEESME](https://gitlab.com/maksym.karpov/department-web-application/-/raw/master/images/Screenshot2.jpg)

## LIST OF DEPARTMENT TAB
![LIST OF DEPARTMENT](https://gitlab.com/maksym.karpov/department-web-application/-/raw/master/images/Screenshot3.jpg)

## SEARCH TAB
![SEARCH](https://gitlab.com/maksym.karpov/department-web-application/-/raw/master/images/Screenshot4.png)

## Department app API:
* GET Employee 

    `curl http://localhost:5000/api/employee/employee_id -v`

    `{
    "id": "5",
    "name": "Maxim Karpov", 
    "dob": "2001-07-26", 
    "salary": 5555, 
    "dep_name": "Kharkiv Department"
    }`

* ADD Employee

    `curl http://localhost:5000/api/employee -d "parameter=description" -X PUT -v`

    `{
    "id": "5",
    "name": "New Employee", 
    "dob": "2001-07-26", 
    "salary": 10000, 
    "dep_name": "New Department"
    }`

* UPDATE Employee

    `curl http://localhost:5000/api/employee/employee_id -d "reqired_parameter=description" -X PATCH -v`

    `{
    "id": "5",
    "name": "Maxim Karpov", 
    "dob": "2001-07-26", 
    "salary": 9999, 
    "dep_name": "Kharkiv Department"
    }`

* DELETE Employee

    `curl http://localhost:5000/api/employee/employee_id -X DELETE -v`

* GET Department 

    `curl http://localhost:5000/api/department/department_id -v`

    `{
    "id": "5",
    "department": "Kharkiv Department"
    }`

* ADD Department

    `curl http://localhost:5000/api/department -d "parameter=description" -X PUT -v`

    `{
    "id": "5",
    "department": "New Department"
    }`

* UPDATE Department

    `curl http://localhost:5000/api/department/department_id -d "reqired_parameter=description" -X PATCH -v`
    
    `{
    "id": "5",
    "department": "Kharkiv Department"
    }`

* DELETE Department
    `curl http://localhost:5000/api/department/department_id -X DELETE -v`

#####  Software quality attributes
Python module unittest was used for correct web app working. Appliaction test coverage is equal to 76%.

