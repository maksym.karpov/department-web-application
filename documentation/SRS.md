# SYSTEM REQUIREMENTS SPECIFICATION

# DEPARTMENT WEB APPLICATION

## Introduction

##### Purpose
Web application is created for managing departments and employees. Application allows to get access for all exsicting information about departments and employees working there. 

##### Document conventions
* OS - Operating System
* DB - Data Base
* Flask - Python microframework
* DOB - Date Of Birth

##### References
* Learning Python, 5th Edition: https://www.oreilly.com/library/view/learning-python-5th/9781449355722/

## Overall Description 

##### Product features
Department web application allows to calculate the avarege salary for each department, browse employees information and quickly find the information about employee by DOB parameter on a certain date or in the period between dates.

##### Operating environment
###### OS: 
* Windows
* Linux
*  MacOS
###### Programming languages:
* Python (Flask)
* HTML5, CSS3
* POSTGRESQL

## System features

##### System feature `LIST OF DEPARTMENTS`
Allows to add, edit and delete Departments.

##### Description and priority
`LIST OF DEPARTMENTS` tab returns HTML-template which display the average salary for each department and provide special tools for customization all existing departments

##### System feature `LIST OF EMPLOYEES`
Allows to track Employees information.

##### Description and priority
`LIST OF EMPLOYEES` tab returns HTML-template which display Departments and associated with it Employees and their salary.

##### System feature `Search`
Allows to track Employees information.

##### Description and priority
`Search` tab returns HTML-template which allows to add new Employees and search by DOB parameter (you can search Employees by exact date or in the peroid between dates). If DOB is correct, the list of Employeesyou will be shown; each Employee parameter can be edited or you can just delete required Employee.


## External interface requirements
Department app API:
* GET Employee 
    `curl http://localhost:5000/api/employee/employee_id -v`
    `{
    "id": "5",
    "name": "Maxim Karpov", 
    "dob": "2001-07-26", 
    "salary": 5555, 
    "dep_name": "Kharkiv Department"
    }`

* ADD Employee
    `curl http://localhost:5000/api/employee -d "parameter=description" -X PUT -v`
    `{
    "id": "5",
    "name": "New Employee", 
    "dob": "2001-07-26", 
    "salary": 10000, 
    "dep_name": "New Department"
    }`

* UPDATE Employee
    `curl http://localhost:5000/api/employee/employee_id -d "reqired_parameter=description" -X PATCH -v`
    `{
    "id": "5",
    "name": "Maxim Karpov", 
    "dob": "2001-07-26", 
    "salary": 9999, 
    "dep_name": "Kharkiv Department"
    }`

* DELETE Employee
    `curl http://localhost:5000/api/employee/employee_id -X DELETE -v`

* GET Department 
    `curl http://localhost:5000/api/department/department_id -v`
    `{
    "id": "5",
    "department": "Kharkiv Department"
    }`

* ADD Department
    `curl http://localhost:5000/api/department -d "parameter=description" -X PUT -v`
    `{
    "id": "5",
    "department": "New Department"
    }`

* UPDATE Department
    `curl http://localhost:5000/api/department/department_id -d "reqired_parameter=description" -X PATCH -v`
    `{
    "id": "5",
    "department": "Kharkiv Department"
    }`

* DELETE Department
    `curl http://localhost:5000/api/department/department_id -X DELETE -v`

#####  Software quality attributes
Python module unittest was used for correct web app working. Appliaction test coverage is equal to 76%.

